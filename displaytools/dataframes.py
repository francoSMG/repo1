from IPython.display import display_html, display, HTML
from itertools import chain,cycle, islice


def display_side_by_side(*args,titles=cycle([''])):
    html_str=''
    for df,title in zip(args, chain(titles,cycle(['</br>'])) ):
        html_str+='<th style="text-align:center"><td style="vertical-align:top">'
        html_str+=f'<h2>{title}</h2>'
        html_str+=df.to_html().replace('table','table style="display:inline"')
        html_str+='</td></th>'
    display_html(html_str,raw=True)


def display_side_by_side_columns(*args,titles=None,columns=1):
    total_dataframes = len(args)
    if not titles :
        titles=['']*total_dataframes
    
    if not total_dataframes == len(titles):
        raise Exception
     
    args_slices = zip(*(iter(args),) * columns)
    title_slices = zip(*(iter(titles),) * columns)

    for arg_slice, title_slice in zip(args_slices,title_slices):
        display_side_by_side(*arg_slice, titles=title_slice)
    
    reminders = total_dataframes % columns
    need_last_row =  reminders != 0
    if need_last_row:
        last_data_frames = args[-reminders:]
        last_titles = titles=titles[-reminders:]
        display_side_by_side(*last_data_frames, titles=last_titles)
    